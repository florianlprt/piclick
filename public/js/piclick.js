const SCORE_STEP = 100

const HIGHLIGHT = new createjs.ColorFilter(255, 255, 255, 0.3, 0, 0, 0, 0);
const HIGHLIGHT_OK = new createjs.ColorFilter(0, 255, 0, 0.4, 0, 0, 0, 0);
const HIGHLIGHT_ERR = new createjs.ColorFilter(255, 0, 0, 0.6, 0, 0, 0, 0);

class PiClick {
  constructor(stage, instructions, score) {
    this.stage = stage;
    this.dom = { instructions, score };
    this.score = 0;
    this.pic = undefined;
  }

  check(event) {
    if (event.target.id == this.pic.id) {
      this.score += SCORE_STEP;
      this.highlight(event, HIGHLIGHT_OK, true);
      this.nextPic();
    } else {
      this.score -= SCORE_STEP;
      this.highlight(event, HIGHLIGHT_ERR, true);
    }
    this.dom.score.innerText = this.score;
  }

  highlight(event, light=false, animate=false) {
    const bitmap = event.target;
    bitmap.alpha = 1;
    bitmap.filters = light ? [light] : [];
    if (animate) {
      createjs.Tween.get(bitmap)
        .to({alpha:1}, 80).to({alpha:0}, 40)
        .to({alpha:1}, 80).to({alpha:0}, 300)
        .call(this.highlight, [event, false, false]);
    }
    bitmap.cache(0, 0, bitmap.image.width, bitmap.image.height);
  }

  initialize() {
    // draw background
    const bg = new createjs.Bitmap(BACKGROUND);
    stage.addChild(bg);
    // draw each element
    ELEMENTS.forEach(el => {
      const { name, img, x, y } = el;
      const bitmap = new createjs.Bitmap(img);
      bitmap.x = x;
      bitmap.y = y;
      bitmap.addEventListener('click', event => this.check(event));
      bitmap.addEventListener('mouseover', event => this.highlight(event, HIGHLIGHT));
      bitmap.addEventListener('mouseout', event => this.highlight(event));
      stage.addChild(bitmap);
    });
  }

  nextPic() {
    this.pic = ELEMENTS.splice(Math.floor(Math.random()*ELEMENTS.length), 1)[0];
    if (this.pic) {
      setTimeout(() => this.speak(), 500);
      this.dom.instructions.innerText = this.pic.name;
    } else {
      this.end();
    }
  }

  speak() {
    responsiveVoice.speak(this.pic.name), 200;
  }

  end() {
    this.stage.children.forEach(child => {
      child.removeAllEventListeners('click');
      this.dom.instructions.innerText = 'Well done !';
    });
  }
}
