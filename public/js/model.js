const BACKGROUND = 'img/full.jpg';

const ELEMENTS = [
  { id: 2, name: 'Aurora', img: 'img/aurora.png', x: 173, y: 91 },
  { id: 3, name: 'A basket', img: 'img/basket.png', x: 203, y: 274 },
  { id: 4, name: 'A blue bird', img: 'img/blue-bird.png', x: 419, y: 100 },
  { id: 5, name: 'A red bird', img: 'img/red-bird.png', x: 378, y :149 }
];
